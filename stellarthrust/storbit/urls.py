from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list/', views.ship_list, name="ship_list"),
    path('stats/<str:ship>/', views.stats, name='stats'),
    path('game/', views.game, name='game'),

    path('utils/stats_turn/', views.stats_turn, name='stats_turn'),
    path('utils/get_ship_data/', views.get_data, name='get_data'),
    path('utils/make_turn/', views.make_turn, name='make_turn'),
    path('utils/get_predicted_path/', views.get_prediction, name='get_prediction'),
    path('utils/transfer/', views.transfer, name='transfer')
]
