from django.shortcuts import render
from django.http import JsonResponse
from .models import Ship as ShipModel, Planet as PlanetModel
from .modules.STCalc import Ship
from .modules import STCalc
import json


def index(request):
    return render(request, 'storbit/index.html')


def ship_list(request):
    ships = ShipModel.objects.order_by('height')
    context = {'ships': ships}
    return render(request, 'storbit/list.html', context)


def stats(request, ship):
    ship_obj = Ship(ShipModel.objects.get(name=ship))
    context = {
        'ship': ship_obj,
        'rd_path': ship_obj.get_rd_path(),
        'path': ship_obj.get_path(),
    }
    return render(request, 'storbit/stats.html', context)


def game(request):
    ships = ShipModel.objects.all()
    planets = PlanetModel.objects.all()
    context = {
        'ships': ships,
        'planets': planets
    }
    return render(request, 'storbit/game.html', context)


def stats_turn(request):
    if request.is_ajax():
        ships = request.POST.get('ships', None).split()
        reset = request.POST.get('reset', None) == 'true'  # converts obtained string (true or false) to boolean
        data = []
        for s in ships:
            db_ship = ShipModel.objects.get(name=s)
            ship = Ship(db_ship)  # Creates Ship object from database data
            ship.make_turn(reset)  # updates orbital elements for Ship object

            rd_path_points = ship.get_rd_path()
            path_points = ship.get_path()

            data.append({
                'ship': s,
                'new_mean': ship.mean,
                'inc': ship.inclination,
                'ra': ship.ra,
                'y_ang': ship.y_angle,
                'rd_path_points': rd_path_points,
                'lon': ship.lon,
                'path_points': path_points,
                'rotation': ship.planet.rotation,
                'icon': str(ship.icon),
                'cartesian_pos': ship.cartesian_position,
            })
            update_database(ship, db_ship)

        return JsonResponse(json.dumps(data), safe=False)


def get_data(request):
    if request.is_ajax():
        ship = Ship(ShipModel.objects.get(name=request.GET.get('ship', None)))
        data = {
            'ship': ship.name,
            'height': ship.height,
            'vel': ship.velocity,
            'y_ang': ship.y_angle,
            'lon': ship.lon,
            'path_points': ship.get_path(),
            'icon': str(ship.icon),
            'planet': str(ship.planet.name),
            'mass_of_planet': ship.planet.mass,
            }
        return JsonResponse(json.dumps(data), safe=False)


def make_turn(request):
    if request.is_ajax():
        ship_n = request.POST.get('ship', None)
        request_type = request.POST.get('type', None)
        turns = 1
        thrust_occurred = False

        db_ship = ShipModel.objects.get(name=ship_n)
        ship = Ship(db_ship)  # Creates Ship object from database data

        if request_type == 'wait':
            turns = int(request.POST.get('val', None))

        if request_type == 'thrust':
            height = float(request.POST.get('val[height]', None))
            nm = float(request.POST.get('val[nm]', None))

            ship.update_path(height, nm)
            thrust_occurred = True

        ship.make_turn(False, turns)
        update_database(ship, db_ship, thrust_occurred)

        data = {
            'ship': ship.name,
            'path_flown': ship.get_latest_path(turns)
        }
        return JsonResponse(json.dumps(data), safe=False)


def get_prediction(request):
    if request.is_ajax():
        pr_ship = Ship(ShipModel.objects.get(name=request.GET.get('ship', None)))
        height = float(request.GET.get('height', None))
        nm = float(request.GET.get('ang_change', None))
        pr_ship.update_path(height, nm)

        data = {
            'predicted_path': pr_ship.get_path()
        }
        return JsonResponse(json.dumps(data), safe=False)


def transfer(request):
    if request.is_ajax():
        db_ship = ShipModel.objects.get(name=request.POST.get('ship', None))
        origin_planet = db_ship.planet.name
        target_planet = request.POST.get('target_planet', None)

        db_ship.planet = PlanetModel.objects.get(name=target_planet)
        db_ship.save()

        data = {
            'new_planet': target_planet,
            'time': STCalc.get_planet_transition_values(origin_planet.lower(), target_planet.lower())[0],
            'dv': STCalc.get_planet_transition_values(origin_planet.lower(), target_planet.lower())[1],
        }
        return JsonResponse(json.dumps(data), safe=False)


def update_database(ship, db_ship, thrust_occurred=False, transfer_occurred=False):
    # Update database by ship object
    if thrust_occurred:
        db_ship.height = ship.height
        db_ship.inclination = ship.inclination
        db_ship.raan = ship.raan
    if transfer_occurred:
        db_ship.planet = ship.planet

    db_ship.mean = ship.mean
    db_ship.planet.rotation = ship.planet.rotation
    # Save updated database
    db_ship.save()
    db_ship.planet.save()
