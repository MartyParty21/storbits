from math import pi, sqrt, pow, sin, asin, cos, acos, tan, atan, radians, degrees, copysign, ceil
from astropy.constants import G, R_earth, M_earth
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import get_body_barycentric
import copy
import datetime

_turn = 15.0  # turn duration in minutes


# static helper methods
def _format_val(val, modulo):
    return val % modulo


# makes a value to be inside (-180; +180)
def _cut_to_180(val):
    if val > 180:
        val -= 360
    if val < -180:
        val += 360
    return round(val, 6)


# computes angular change
def _get_ang_change(ang_vel, time):
    return ang_vel * time


# Returns angle opposite to angle-side-angle of spherical triangle if one of the angles is 90 degrees
def _get_opposite_ang(a, s):
    ang = radians(a)
    s = radians(s)
    return degrees(acos(sin(ang) * cos(s)))


class Planet(object):
    def __init__(self, planet):
        self.__name = planet
        self.__rotation = planet.rotation
        self.__ang_vel = planet.ang_vel
        self.__radius = R_earth.value * planet.radius
        self.__mass = M_earth.value * planet.mass

    @property
    def name(self):
        return self.__name

    @property
    def rotation(self):
        return self.__rotation

    @property
    def ang_vel(self):
        return self.__ang_vel

    @property
    def radius(self):
        return self.__radius

    @property
    def mass(self):
        return self.__mass

    def rotate(self, reset, t=_turn):
        if reset:
            self.__rotation = 0.0
        else:
            self.__rotation += _get_ang_change(self.__ang_vel, t)
        if self.__rotation > 360:
            self.__rotation -= 360


class Ship(object):
    def __init__(self, ship):
        self.__name = ship.name
        self.__height = ship.height
        self.__inclination = ship.inclination
        self.__raan = ship.raan
        self.__mean = ship.mean
        self.__icon = ship.icon
        self.__planet = Planet(ship.planet)
        self.__period = self.__get_period()
        self.__velocity = self.__get_velocity()
        self.__y_angle = self.__get_y_angle()
        self.__ra = self.__get_ra()
        self.__lon = self.__get_lon()
        self.__cartesian_position = self.__get_cartesian()

    # Updates mean and changes values that change with time accordingly
    def make_turn(self, reset, turns=1.0):
        __time = turns * _turn
        self.__planet.rotate(reset, __time)
        self.__mean = self.__count_mean(reset, __time)
        self.__y_angle = self.__get_y_angle()
        self.__ra = self.__get_ra()
        self.__lon = self.__get_lon()
        self.__cartesian_position = self.__get_cartesian()

    # Updates path after thrust
    def update_path(self, height, nm):
        rel_ra = self.__get_rel_ra()
        ang = _get_opposite_ang(self.inclination, rel_ra)

        new_ang = ang - nm
        new_inc = _get_opposite_ang(new_ang, self.y_angle)
        rang = radians(new_ang)
        rinc = radians(new_inc)

        new_rel_ra = degrees(acos(cos(rang) / sin(rinc)))
        new_rel_ra = copysign(new_rel_ra, rel_ra)

        new_mean = degrees(acos((cos(rang) * cos(rinc)) / (sin(rang) * sin(rinc))))
        if new_rel_ra < 0:
            new_mean = 360 - new_mean

        new_raan = _cut_to_180(self.ra - new_rel_ra)

        # Set new values
        self.__height = height
        self.__mean = new_mean
        self.__inclination = new_inc
        self.__raan = new_raan

        self.__period = self.__get_period()
        self.__velocity = self.__get_velocity()

    @property
    def name(self):
        return self.__name

    @property
    def mean(self):
        return self.__mean

    @property
    def height(self):
        return self.__height

    @property
    def inclination(self):
        return self.__inclination

    @property
    def raan(self):
        return self.__raan

    @property
    def y_angle(self):
        return self.__y_angle

    @property
    def ra(self):
        return self.__ra

    @property
    def lon(self):
        return self.__lon

    @property
    def icon(self):
        return self.__icon

    @property
    def velocity(self):
        return self.__velocity

    @property
    def planet(self):
        return self.__planet

    @property
    def cartesian_position(self):
        return self.__cartesian_position

    def __get_period(self):
        return 2 * pi * sqrt(pow(self.__planet.radius + float(self.__height * 1000), 3) / (G.value * self.__planet.mass)) / 60

    def __get_velocity(self):
        return sqrt(G.value * self.__planet.mass / (self.__planet.radius + float(self.__height * 1000)))

    def __count_mean(self, reset, time):
        if reset:
            _new_mean = 0
        else:
            _ang_vel = 360 / self.__period
            _new_mean = _format_val(self.__mean + _get_ang_change(_ang_vel, time), 360)
        return round(_new_mean, 6)

    # Returns angle which stand for both Declination and Latitude
    def __get_y_angle(self):
        mean = radians(self.__mean)
        inc = radians(self.__inclination)
        return round(degrees(asin(sin(mean) * sin(inc))), 6)

    # Returns absolute RA (raan + relra)
    def __get_ra(self):
        ra = float(self.__raan) + self.__get_rel_ra()
        return _cut_to_180(ra)

    # Returns relative RA(-180; 180) => relative to raan
    def __get_rel_ra(self):
        mean = radians(self.__mean)
        inc = radians(self.__inclination)
        dec = radians(self.__y_angle)
        if self.__inclination == 90:
            inc = radians(89.99999)
        relra = degrees(2 * atan(tan((mean - dec) / 2) * (sin((pi / 2 + inc) / 2) / sin((pi / 2 - inc) / 2))))
        return relra

    # Returns longitude
    def __get_lon(self):
        lon = self.ra - float(self.planet.rotation)
        if lon > 180:
            lon -= 360
        if lon < -180:
            lon += 360
        return round(lon, 6)

    # Returns position of ship in cartesian coordinates with center in center of planet [m]
    def __get_cartesian(self):
        r = self.planet.radius + self.__height * 1000
        __cart = {
            'x': r * cos(radians(self.__y_angle)) * cos(radians(self.__ra)),
            'y': r * cos(radians(self.__y_angle)) * sin(radians(self.__ra)),
            'z': r * sin(radians(self.__y_angle))
        }
        return __cart

    # Returns path of ship, latest means the function returns only path from last turn
    def _generate_path_points(self, is_relative_to_earth=True, latest=False, turns=1):
        coord_points = []
        # Make a copy of a ship to be able to maniplate with its parameters for path generation
        path_ship = copy.deepcopy(self)
        duration = path_ship.__period

        if latest:
            path_ship.make_turn(False, -turns)
            duration = _turn * turns
        elif is_relative_to_earth:
            # Sets path_ships to previous orbit in order to draw from -period to +period
            path_ship.make_turn(False, -self.__period / _turn)
            duration *= 2

        # Amount of points generated
        draw_fraction = 128
        for i in range(0, draw_fraction + 1):
            path_ship.make_turn(False, duration / (_turn * draw_fraction))

            if is_relative_to_earth:
                x = path_ship.__lon
            else:
                x = path_ship.__ra

            point = [x, path_ship.__y_angle]
            coord_points.append(point)
        return coord_points

    def get_path(self):
        return self._generate_path_points()

    def get_latest_path(self, turns):
        return self._generate_path_points(latest=True, turns=turns)

    def get_rd_path(self):
        return self._generate_path_points(False)


# ------------------Planet transition part-------------------
_v_max = 150000  # km/s
_a = 10  # km/s^2
_t_to_maxv = _v_max / _a
_s_to_maxv = (_a * _t_to_maxv ** 2) / 2
# converts astronomical unit to km
_au_to_km = u.au.to(u.kilometer)


# Returns flight time in turns and delta-v used for transfer from planet p1 to planet p2
def get_planet_transition_values(p1, p2='earth', t=Time(datetime.datetime.now())):
    primary_flight_time = __get_flight_time(p1, p2, t, t)
    while True:
        t_at_dest = t + primary_flight_time * u.second
        flight_time = __get_flight_time(p1, p2, t, t_at_dest)

        # Breaks the loop if next iteration doesn't have a possibility to change the flight time by a turn
        if abs(flight_time - primary_flight_time) < _turn - (flight_time % _turn):
            break
        primary_flight_time = flight_time

    if flight_time > _t_to_maxv * 2:
        dv = 2 * _v_max
    else:
        dv = _a * flight_time

    return ceil(flight_time / _turn), dv


def __get_distance(p1, p2, t, t_at_dest):
    p1 = get_body_barycentric(p1, t).get_xyz()
    p2 = get_body_barycentric(p2, t_at_dest).get_xyz()
    dst = ((p1[0].value - p2[0].value) ** 2 + (p1[1].value - p2[1].value) ** 2 + (p1[2].value - p2[2].value) ** 2) ** (1 / 2) * _au_to_km
    return dst


def __get_flight_time(p1, p2, t, t_at_dest):
    dst = __get_distance(p1, p2, t, t_at_dest)
    if (dst / 2 - _s_to_maxv) > 0:
        return ((dst / 2) - _s_to_maxv) / _v_max + 2 * _t_to_maxv
    else:
        return 2 * (dst / _a) ** (1 / 2)
