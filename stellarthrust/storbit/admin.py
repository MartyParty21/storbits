from django.contrib import admin
from .models import Ship, Icon, Planet

admin.site.register(Ship)
admin.site.register(Icon)
admin.site.register(Planet)
