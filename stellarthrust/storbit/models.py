from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Planet(models.Model):
    name = models.CharField(max_length=20)
    rotation = models.FloatField(default=0, validators=[
            MaxValueValidator(180),
            MinValueValidator(-180)
        ])
    ang_vel = models.FloatField(default=0.25)
    # radius and mass are relative to earth
    radius = models.FloatField(default=1)
    mass = models.FloatField(default=1)

    def __str__(self):
        return self.name


class Icon(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name + '.png'


class Ship(models.Model):
    name = models.CharField(max_length=30)
    height = models.FloatField(default=300.0, validators=[
        MaxValueValidator(70000),
        MinValueValidator(100)
    ])
    inclination = models.FloatField(default=45, validators=[
            MaxValueValidator(180),
            MinValueValidator(0)
        ])
    raan = models.FloatField(default=0, validators=[
            MaxValueValidator(180),
            MinValueValidator(-180)
        ])
    mean = models.FloatField(default=0, validators=[
            MaxValueValidator(360),
            MinValueValidator(0)
        ])
    icon = models.ForeignKey(Icon, default=3, on_delete=models.CASCADE)
    planet = models.ForeignKey(Planet, default=3, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
