$(document).ready(function () {
    //------------------------------------
    //Django basic setup for accepting ajax requests.
    // Cookie obtainer Django

    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    const csrftoken = getCookie('csrftoken');

    // Setup ajax connections safetly

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });
});

// updates mean for a list of ships, renders a path if draw = true
function statsTurn(ships, reset, draw) {
    $.ajax({
    url: '/storbit/utils/stats_turn/',
    data: {
        'ships': ships,
        'reset': reset,
    },
    dataType: 'json',
    type: 'POST',
    success: function(dat) {
        const data = JSON.parse(dat);
        for(let i = 0; i < data.length; i++) {
            $('#'+data[i].ship+'-mean').text(data[i].new_mean + '°');
                if(draw) {
                    $('#'+data[i].ship+'-ra').text(data[i].ra + '°');
                    $('#'+data[i].ship+'-dec').text(data[i].y_ang + '°');
                    $('#'+data[i].ship+'-lon').text(data[i].lon + '°');
                    $('#'+data[i].ship+'-lat').text(data[i].y_ang + '°');
                    $('#'+data[i].ship+'-rot').text(data[i].rotation + '°');
                    $('#'+data[i].ship+'-x').text(data[i].cartesian_pos.x);
                    $('#'+data[i].ship+'-y').text(data[i].cartesian_pos.y);
                    $('#'+data[i].ship+'-z').text(data[i].cartesian_pos.z);

                    drawPath('rd-', data[i].rd_path_points, data[i].ra, data[i].y_ang, data[i].icon);
                    drawPath('', data[i].path_points, data[i].lon, data[i].y_ang, data[i].icon);
                }
            }
        }
    });
}

function drawPath(id, path_points, x, y, icon) {
    $('#' + id + 'path-svg').remove();
    const draw = SVG(id + 'path-div').id(id + 'path-svg').viewbox(-180, -90, 360, 180);

    // draw icon on position
    const image = draw.image('/static/storbit/images/icons/' + icon);
    image.size(40, 40).move(x - 20, -y - 20);

    const rd = getPathFromPoints(path_points);
    draw.path(rd).id(id + 'path').addClass('path');
}

function getPathFromPoints(path_points) {
    // declination must be negative due to svg reversed y axis (compared to pos points)
    let d = 'M ' + path_points[0][0] + ' ' + -path_points[0][1];
    for (i = 1; i < path_points.length; i++) {

        // if the orbit part is prograde and transfers to other side of the svg
        const a = (path_points[i - 1][0] - path_points[i][0] > 180);


        // if the orbit part is retrograde and transfers to other side of the svg
        const b = (path_points[i][0] - path_points[i - 1][0] > 180);

        // Adds points behind the svg borders to create no-stop line
        if (a) {
            d += ' L ' + (path_points[i][0]+360) + ' ' + -path_points[i][1];
            d += ' M ' + (path_points[i-1][0]-360) + ' ' + -path_points[i-1][1];
        }
        if(b) {
            d += ' L ' + (path_points[i][0]-360) + ' ' + -path_points[i][1];
            d += ' M ' + (path_points[i-1][0]+360) + ' ' + -path_points[i-1][1];
        }

        d += ' L ' + path_points[i][0] + ' ' + -path_points[i][1];
    }
    return d;
}