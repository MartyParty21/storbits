let ship = {
};
let newPathVal = {
    height: 0,
    nm: 0,
};

const dest = [(Math.random() * 360) - 180, (Math.random() * 180) - 90], G = 6.67408e-11,
    maxDeltaV = 500;
let deltaV = 0, deltaVUsed = 0, turns = 0, map_drawn = false, draw, M;

$(document).ready(function () {
    draw = SVG('game-path-div').id('game-path-svg').viewbox(-180, -90, 360, 180);
    drawGoal();
    $('#start-dialog').dialog({
        modal: true,
        closeOnEscape: false,
        width: 600,
        open: function(event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
    $('#transfer-dialog').dialog({
        modal: true,
        autoOpen: false
    });
    $('#end-dialog').dialog({
        modal: true,
        closeOnEscape: false,
        autoOpen: false,
        open: function(event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
});


function startGame () {
    const shipSelect = $('#ship-select').val(); // get selected value
    $('#start-dialog').dialog('close');
    $('#ship').html(shipSelect);
    getShipData(shipSelect)
}

function endGame() {
    $('#dv-result').html(deltaVUsed.toFixed(2) + ' m/s');
    $('#time-result').html(turns + ' turns');
    $('#end-dialog').dialog('open');
}

function getShipData (shipName) {
    $.ajax({
        url: '/storbit/utils/get_ship_data/',
        data: {
            'ship': shipName
        },
        dataType: 'json',
        type: 'GET',
        success: function(dat) {
            const data = JSON.parse(dat);
            ship.name = data.ship;
            ship.height =  data.height;
            ship.velocity= data.vel;
            ship.x = data.lon;
            ship.y = data.y_ang;
            ship.pathPoints = data.path_points;
            ship.icon = data.icon;
            ship.planet = data.planet;
            M = data.mass_of_planet;
            reloadElements()
        }
    });
}

function reloadElements () {
    if(!map_drawn) {
        let img_path = '/static/storbit/images/planets/' + ship.planet.toLowerCase() + '.png'
        $('#game-path-div').css('background-image', 'url("' + img_path + '")');
        map_drawn = true
    }
    newPathVal.height = ship.height;
    newPathVal.nm = 0;
    deltaV = 0;
    $('#height-range').slider({
        min: getMaxHeightChange(false),
        max: getMaxHeightChange(true),
        value: ship.height,
        step: 0.1,
        slide: function (event, ui) {
            newPathVal.height = parseFloat(ui.value);
            updateThrust()
            }
        });
    $('#nm-range').slider({
        min: -getMaxAngularChange(),
        max: getMaxAngularChange(),
        value: 0,
        step: 0.01,
        slide: function (event, ui) {
            newPathVal.nm = parseFloat(ui.value);
            updateThrust();
            }
        });

    $('#dv-used').html(deltaVUsed.toFixed(1));
    $('#trn-passed').html(turns);

    $('#deltav').html(maxDeltaV.toFixed(1) + ' m/s');
    $('#nm').html(newPathVal.nm);
    $('#height').html(newPathVal.height);

    drawPath('game-', ship.pathPoints, ship.x, ship.y, ship.icon);
    draw = SVG.get('game-path-svg');
    drawGoal();
    if(checkGoalPass()) {
        endGame();
    }
}

function getMaxAngularChange() {
    return  (Math.atan(maxDeltaV/ship.velocity) * 180 / Math.PI).toFixed(2)
}

function getMaxHeightChange(isMax) {
    let dv = -maxDeltaV;
    if (isMax) {
        dv = maxDeltaV
    }
    let height = (6378 + ship.height) * 1000;
    let o = G * M;

    let x = dv / 2 * Math.sqrt(height) / Math.sqrt(o) + 1;
    let mh = ((Math.pow(x, 2) * height) / (2- Math.pow(x, 2)) - 6378000) / 1000;
    if (mh < 100 ) {
        mh = 100
    }
    if(mh > 70000) {
        mh = 70000
    }
    return parseFloat(mh.toFixed(1))
}


function updateThrust() {
    $('#height').html(newPathVal.height);
    $('#nm').html(newPathVal.nm);

    let ang = newPathVal.nm;
    let newHeight = (newPathVal.height + 6378) * 1000;
    let height = (ship.height + 6378) * 1000;
    let o = G * M;

    let angTh = Math.abs(Math.tan(ang * Math.PI / 180) * ship.velocity);
    let hTh = Math.abs(2 * Math.sqrt(o / height)*(Math.sqrt(2 * newHeight /(newHeight + height)) - 1));
    deltaV = angTh + hTh;

    $('#deltav').html((maxDeltaV - deltaV).toFixed(1) + ' m/s');
    drawPredictedPath();
    checkIfTurnPossible(false);
}

function toggleDivs(){
    let sel = $("input[name='controlCheckbox']:checked").val();
    if (sel == 0) {
        $('#form-wait').addClass('disabled');
        $('#form-thrust').removeClass('disabled');
        checkIfTurnPossible(false);
    }
    else {
        $('#form-thrust').addClass('disabled');
        $('#form-wait').removeClass('disabled');
        checkIfTurnPossible(true);
    }
}

function checkIfTurnPossible (divIsWait) {
    let possible = false;
    if (divIsWait || deltaV <= maxDeltaV) {
        possible = true;
    }

    let btn = $('#turn-btn');
    if (possible && btn.hasClass('disabled')) {
        btn.removeClass('disabled');
    }
    else if (!possible && !btn.hasClass('disabled')) {
        btn.addClass('disabled');
    }
}

function makeTurn() {
    let action = $("input[name='controlCheckbox']:checked").val();
    let data = {
        'ship': ship.name,
        'type': '',
        'val': {}
    };
    if (action == 0) {
        data.type = 'thrust';
        data.val = {
            'height': newPathVal.height,
            'nm': newPathVal.nm
        }
    }
    else if (action == 1 ) {
        data.type = 'wait';
        data.val = $('#turn-select').val();
    }
    $.ajax({
        url: '/storbit/utils/make_turn/',
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function(dat) {
            const data = JSON.parse(dat);
            ship.recentPath = data.path_flown;
            deltaVUsed += deltaV;
            turns += parseInt($('#turn-select').val());
            getShipData(ship.name);
        }
    });
}

function drawPredictedPath() {
    let data = {
        'ship': ship.name,
        'height': newPathVal.height,
        'ang_change': newPathVal.nm
    };
    $.ajax({
        url: '/storbit/utils/get_predicted_path/',
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function(dat) {
            const data = JSON.parse(dat);

            $('#game-path-prediction').remove();
            const rd = getPathFromPoints(data.predicted_path);
            draw.path(rd).id('game-path-prediction').addClass('path-prediction');
        }
    });
}

function drawGoal() {
    const radialLength = 5;
    const cross =
        'M ' + (dest[0] - radialLength) + ' ' + (dest[1] - radialLength) + ' L ' + (dest[0] + radialLength)
        + ' ' + (dest[1] + radialLength)
        + ' M ' + (dest[0] + radialLength) + ' ' + (dest[1] - radialLength) + ' L ' + (dest[0] - radialLength)
        + ' ' + (dest[1] + radialLength);
    draw.path(cross).addClass('goal-marker');
    draw.circle(2*radialLength).move(dest[0]-radialLength, dest[1]-radialLength).addClass('goal-marker')
}

function checkGoalPass() {
    let passed = false;
    if (ship.recentPath) {
        ship.recentPath.forEach(function(point) {
            let distanceToGoal = Math.sqrt(Math.pow(point[0] - dest[0], 2) + Math.pow(point[1] + dest[1], 2));
            if(distanceToGoal < 5 ) {
                passed = true
            }
        });
    }
    return passed;
}

function showTransferDialog() {
    $('#transfer-dialog').dialog('open');
}

function transfer() {
    const planetSelect = $('#planet-select').val(); // get selected value
    $('#transfer-dialog').dialog('close');
    let data = {
        'ship': ship.name,
        'target_planet': planetSelect,
    };

    $.ajax({
        url: '/storbit/utils/transfer/',
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function(dat) {
            const data = JSON.parse(dat);
            ship.planet = data.new_planet;
            turns += data.time;
            deltaVUsed += data.dv;
            map_drawn = false;
            getShipData(ship.name);
        }
    });
}
